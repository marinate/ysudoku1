package controller;
import javax.swing.SwingUtilities;

import view.*;
import model.*;

public class Sudoku implements Runnable {
    static Sudoku sd;

    public static void main(String[] args) {
        sd = new Sudoku();
        SwingUtilities.invokeLater(sd);
    }

    public void run() {
        SudokuMainView sm = new SudokuMainView();
        sm.start();
    }
}
