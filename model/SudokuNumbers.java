package model;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class SudokuNumbers {
    private int n;
    private Element[][] elements;
    private int debug = 0;
    private String blanksquare = ".";
    private Handlist handlist;
    private String[] vallist;
    private int debug0 = 1;
    private String title;
    private String filename;
    private Check ck;
    private Hint hint;

    public SudokuNumbers() {
        this(9);
    }

    public SudokuNumbers(int nn) {
        n = nn;
        vallist = new String[n+1];
        for (int i = 0; i <= n; i++) {
            if (i == 0)
                vallist[i] = " ";
            else
                vallist[i] = String.valueOf(i);
        }

        elements = new Element[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
               elements[i][j] = new Element(0, Element.NULL);
            }
        }
        ck = new Check(this);
        hint = new Hint(this);
        handlist = new Handlist();
    }

    public int setInitialdata(String idata) {
        String[] ldata = idata.split("\n");
        int rc = 0;
        int ii, jj, vv;
 
        if (idata != null && !idata.equals("")) {
            for (String str : ldata) {
                String[] e = str.split(",|:");
                ii = Integer.parseInt(e[0]);
                jj = Integer.parseInt(e[1]);
                vv = Integer.parseInt(e[2]);
                rc = checkval(ii, jj, vv);
                if (rc >= 0) {
                    elements[ii-1][jj-1].setValStatus(vv, Element.PREDEF);
                    //handlist.add(new Hand('I', ii, jj, vv));
                } else {
                    System.out.printf("%d %d %d is out of range\n", ii, jj, vv);
                }
            }
        }
        return rc;
    }

    public int setInitialdata(String[] idata) {
        int rc = 0;
        int i, j, k, v;
        String line;

        if (idata.length != n) {
            System.out.println("Size is not " + n + "  (" + idata.length + ")");
            return -1;
        }
        for (i = 0; i < n; i++) {
            line = idata[i];
            String e[] = line.split(" +");
            for (j = 0;  j < n; j++) {
                v = Integer.valueOf(e[j+1]);
                if (v != 0) {
                    elements[i][j].setValStatus(v, Element.PREDEF);
                    //handlist.add(new Hand('I', i, j, v));
                } else {
                    elements[i][j].setValStatus(0, Element.NULL);                  
                }
            }
        }
        return rc;
    }

    public void initialize() {
        int i, j;
        for (i = 0; i < n; i++) {
            for (j = 0; j < n; j++) {
                elements[i][j].setValStatus(0, Element.NULL);
            }
        }
        handlist.init();
    }

    public void reset() {
        int i, j;
        for (i = 0; i < n; i++) {
            for (j = 0; j < n; j++) {
                if (elements[i][j].status() != Element.PREDEF) {
                    elements[i][j].setValStatus(0, Element.NULL);
                }
            }
        }
        handlist.init();
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String title() {
        return title;
    }

    public void setFilenmae(String filename) {
        this.filename = filename;
    }

    public String filename() {
        return filename;
    }

    public void print(String msg) {
        int i, j;
        int val, status;
        String str, vstr;
        System.out.println(msg);
        System.out.print("  ");
        for (i = 0; i < n; i++) {
            System.out.printf(" %d", i + 1);
            if (i % 3 == 2)
                System.out.print(" ");
        }
        System.out.println();
        System.out.println();
        for (i = 0; i < n; i++) {
            System.out.printf("%d ", i + 1);
            for (j = 0; j < n; j++) {
                val = elements[i][j].val();
                status = elements[i][j].status();
                if (status == Element.NULL)
                    vstr = blanksquare;
                else
                    vstr = String.format("%d", val);
                str = " ";
                if (status == Element.PREDEF)
                    str = "*";
                else if (status == Element.SETTING)
                    str = "+";
                else if (status == Element.DELETING)
                    str = "-";
                System.out.printf("%s%s", str, vstr);
                if (j % 3 == 2)
                    System.out.print(" ");
            }
            System.out.println();
            if (i % 3 == 2)
                System.out.println();
        }
    }

    public int checkxyval(int x, int y) {
        int rc = 0;
        if (x > 0 && x <= n) {
            if (y > 0 && y <= n) {
                rc = 1;
            } else {
                rc = -2;
            }
        } else {
            rc = -1;
        }
        return rc;
    }

    public int checkvval(int v) {
        int rc = 0;
        if (v >= 0 && v <= n) {
            rc = 1;
        } else {
            rc = -3;
        }
        return rc;
    }

    public int checkval(int x, int y, int v) {
        int rc = checkxyval(x, y);
        if (rc >= 0) {
            rc = checkvval(v);
        }
        return rc;
    }

    // s : cmd x y v  or no cmp x y v
    public int putNumstr(String s) {
        String[] e;
        char cmd;
        int i, x, y;
        String v = "";
        //System.out.println("putNumstr=" + s);
        e = s.split(" +");
        i = 0;
        if (Character.isDigit(e[0].charAt(0))) {
            i = 1;
        }
        cmd = e[i].charAt(0);
        x = Integer.valueOf(e[i+1]);
        y = Integer.valueOf(e[i+2]);
        if (cmd == 'd')
            v =  " ";
        else
            v = e[i+3];
        if (v.equals("-"))
            v = " ";
        return putNums(x, y, v);
    }

    public int putNums(int x, int y, String v) {
        int rc = 0;
        int i;
        for (i = 0; i <= n; i++) {
            if (vallist[i].equals(v)) {
                break;
            }
        }
        if (i == 0 || v.equals("0")) {
            rc = delNum(x, y);
        } else if (i <= n) {
            rc = putNum(x, y, i);
        } else {
            rc = -4;
        }
        return rc;
    }

    public int putNum(int x, int y, int v) {
        int rc = 0;
        int preval;
        //System.out.println(" putNum(" + x + ", " + y + "  =" + v);
        rc = checkxyval(x, y);
        if (rc >= 0) {
            rc = checkvval(v);
            if (rc >= 0) {
                x--;
                y--;
                if (elements[x][y].status() != Element.PREDEF) {
                    preval = elements[x][y].val();
                    if (preval != v) {
                        elements[x][y].setVala(v);
                        handlist.add('p', x+1, y+1, elements[x][y].val());
                    }
                } else {
                    rc = -4;
                }
            } else {
                rc = -5;
            }
        } else {
            rc = 6;
        }
        return rc;
    }

    public int delNum(int x, int y) {
        int rc = 0;
        //System.out.println(" delNum(" + x + ", " + y);
        rc = checkxyval(x, y);
        if (rc > 0) {
            x--;
            y--;
            if (elements[x][y].status() != Element.NULL) {
                elements[x][y].setValStatus(0, Element.DELETING);
                elements[x][y].setVala(0);
                handlist.add('p', x+1, y+1, elements[x][y].val());
            }
        } else {
            rc = -4;
        }
        return rc;
    }

    public void confirm() {
        int i, j;
        for (i = 0; i < n; i++) {
            for (j = 0; j < n; j++) {
                if (elements[i][j].status() == Element.SETTING) {
                    elements[i][j].setStatus(Element.FIXED);
                    handlist.add('p', i+1, j+1, elements[i][j].val());
                }
                if (elements[i][j].status() == Element.DELETING) {
                    elements[i][j].setValStatus(0, Element.NULL);
                    handlist.add('d', i+1, j+1, elements[i][j].val());
                }
            }
        }
    }

    public List<ValuePos> check() {
        return ck.check();
    }

    public int[][] hint() {
        return hint.getHint(1);
    }
    public void cancel() {
        int i, j;
        for (i = 0; i < n; i++) {
            for (j = 0; j < n; j++) {
                if (elements[i][j].status() == Element.SETTING) {
                    elements[i][j].setValStatus(0, Element.NULL);
                }
                if (elements[i][j].status() == Element.DELETING) {
                    elements[i][j].setStatus(Element.FIXED);
                }
            }
        }
    }

    public int n() {
        return n;
    }

    public int getValue(int x, int y) {
        int rc = checkval(x+1, y+1, 1);
        if (rc >= 0)
            rc = elements[x][y].val();
        return rc;
    }

    public int getStatus(int x, int y) {
        int rc = checkval(x+1, y+1, 1);
        if (rc >= 0)
            rc = elements[x][y].status();
        return rc;
    }

    public Handlist getHandlist() {
        return handlist;
    }

    public String[] vallist() {
        return vallist;
    }

    public void forward() {
        forwardback(1);
    }

    public void backword() {
        forwardback(-1);
    }

    private void forwardback(int d) {
        int nc = handlist.changecuriddiff(d);
        if (nc < 0) {
            //System.out.println("NC=" + nc);
            return;
        }
        int i, j;
        for (i = 0; i < n; i++) {
            for (j = 0; j < n; j++) {
                if (elements[i][j].status() != Element.PREDEF) {
                    elements[i][j].setValStatus(0, Element.NULL);
                }
            }
        }
        List<Hand> hands = handlist.getHans();
        for (Hand h : hands) {
            if (h.no() > nc) {
                break;
            }
            i = h.x() - 1;
            j = h.y() - 1;
            elements[i][j].setVala(h.v());
        }
    }
    public void dumpHanlist(int all) {
        int i = 0;
        System.out.println("------handlist------");
        for (Hand h : handlist.getHans()) {
            if (all == 1 || h.cmd() != 'I') {
                i++;
                System.out.println(String.valueOf(i) + " " + h);
            }
        }
        System.out.println("----------------");
    }
    public static void main(String[] args) {
        String initdata = "1,4:4\n1,6:6\n" +
                          "2,1:7\n2,2:4\n2,8:8\n2,9:2\n" +
                          "3,2:9\n3,4:2\n3,6:3\n3,8:6\n" +
                          "4,1:8\n4,3:1\n4,7:2\n4,9:4\n" +
                          "6,1:5\n6,3:4\n6,7:1\n6,9:9\n" +
                          "7,2:7\n7,4:9\n7,6:2\n7,8:4\n" +
                          "8,1:4\n8,2:1\n8,8:9\n8,9:6\n" +
                          "9,4:1\n9,6:8\n";
        SudokuNumbers sn = new SudokuNumbers();
        Check ck = new Check(sn);
        //sn.setInitialdata("1,1:8\n1,2:9\n1,8:8\n1,3:3\n1,7:3\n3,1,9");
        sn.setInitialdata(initdata);
        int x, y, v;
        List<ValuePos> elist;

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String instr;
        char cmd;

        while (true) {
            elist = ck.check();
            sn.print("MAIN");
            if (elist == null) {
                System.out.println("====DONE=====");
            } else {
                for (ValuePos el : elist) {
                    el.print(" ");
                }
                System.out.println();
            }

            System.out.print("cmd(p d c q)");
            instr = "";
            try {
                instr = br.readLine();
            } catch(IOException e) {
            }

            cmd = instr.charAt(0);
            if (cmd == 'q')
                break;

            String[] ldata = instr.split(" ");
            if (cmd == 'p') {
                x = Integer.parseInt(ldata[1]);
                y = Integer.parseInt(ldata[2]);
                v = Integer.parseInt(ldata[3]);
                sn.putNum(x, y, v);
            } else if (cmd == 'd') {
                x = Integer.parseInt(ldata[1]);
                y = Integer.parseInt(ldata[2]);
                sn.delNum(x, y);
            } else if (cmd == 'c') {
                sn.confirm();
            } else if (cmd == 'u') {
                sn.cancel();
            } else if (cmd == 'l') {
                List<Hand> hlist = sn.getHandlist().getHans();
                for (Hand h : hlist) {
                    System.out.println(h);
                }
            }
        }
    }
}
