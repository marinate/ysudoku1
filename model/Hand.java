package model;

public class Hand {
    private char cmd;
    private int no;
    private int x;
    private int y;
    private int v;

    public Hand(char cmd, int no, int x, int y, int v) {
        this.cmd = cmd;
        this.no = no;
        this.x = x;
        this.y = y;
        this.v = v;
    }

    public Hand(char cmd, int no, int x, int y) {
        this(cmd,no,  x, y, 1);
    }

    public char cmd() {
        return cmd;
    }

    public int no() {
        return no;
    }

    public int x() {
        return x;
    }

    public int y() {
        return y;
    }

    public int v() {
        return v;
    }

    public String toString() {
        String s = "" + no + " " + cmd +  " [" + x + ", " + y;
        if (cmd != 'd')
            s += "  " + v;
        return s + "]";
    }
}
