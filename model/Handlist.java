package model;

import java.util.List;
import java.util.ArrayList;

public class Handlist {
    private int curid;
    private int lastid;
    private List<Hand> hands;

    public Handlist() {
        init();
    }

    public void init() {
        hands = new ArrayList<>();
        curid = 0;
        lastid = 0;
    }

    public void add(char cmd, int x, int y, int val) {
        if (curid <= lastid) {
            List<Hand> nhands = new ArrayList<>();
            for (int i = 0; i < curid; i++) {
                nhands.add(hands.get(i));
            }
            hands = nhands;
        }
        curid++;
        hands.add(new Hand(cmd, curid, x, y, val));
        lastid = curid;
    }

    public List<Hand> getHans() {
        return hands;
    }

    public int curid() {
        return curid;
    }

    public int lastid() {
        return lastid;
    }

    public int changecuriddiff(int d) {
        int nc = curid + d;
        if (nc < 0 || nc > lastid) {
            return -1;
        }
        curid = nc;
        return nc;
    }
}
