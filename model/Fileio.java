package model;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;



public class Fileio {

    public List<ValuePos>  newhand(SudokuNumbers sn, File f) {
        int i, n;
        String line;
        String[] idata;
        List<ValuePos> elist = null;

        n = sn.n();
        //System.out.println("FILE=" + f.getAbsolutePath());
        try (FileReader fr = new FileReader(f);BufferedReader br = new BufferedReader(fr)) {
            // read headline
            line = br.readLine();
            String title = line.substring(2);
            sn.setTitle(title);
            sn.setFilenmae("");
            // read n (skip)
            line = br.readLine();
            // read init data
            idata = new String[n];
            i = 0;
            while (i < n) {
                line = br.readLine();
                if (line.charAt(0) == '#') {
                    continue;
                }
                idata[i++] = line;
            }
            sn.initialize();
            sn.setInitialdata(idata);
            elist = sn.check();
        } catch (IOException ioe) {
            System.err.println(ioe);
        } finally {
            return elist;
        }
    }

    public List<ValuePos> openhand(SudokuNumbers sn, File f) {
        int i, n;
        String line;
        String[] idata;
        List<ValuePos> elist = null;

        n = sn.n();
        //System.out.println("OPEN:" + f.getAbsolutePath());
        try (FileReader fr = new FileReader(f);BufferedReader br = new BufferedReader(fr)) {
            // read headline
            line = br.readLine();
            String title = line.substring(2);
            sn.setTitle(title);
            sn.setFilenmae(f.getName());
            // read n (skip)
            line = br.readLine();
            // read init data
            idata = new String[n];
            i = 0;
            while (i < n) {
                line = br.readLine();
                if (line.charAt(0) == '#') {
                    continue;
                }
                idata[i++] = line;
            }
            sn.initialize();
            sn.setInitialdata(idata);

            while ((line = br.readLine()) != null) {
                if (line.charAt(0) == '#') {
                    continue;
                }
                //System.out.println(line);
                sn.putNumstr(line);
                sn.confirm();
            }
            elist = sn.check();
        } catch (IOException ioe) {
            System.err.println(ioe);
        } finally {
            return elist;
        }
    }

    public void savehand(SudokuNumbers sn, File f) {
        int i, j, v, n;
        String line;
        n = sn.n();
        System.out.println("SAVE:" + f.getAbsolutePath());
        try (FileWriter fr = new FileWriter(f)) {
            Handlist handlist;
            line = String.format("## %s\n", sn.title());
            fr.write(line);
            line = String.format("%d\n", n);
            fr.write(line);
            for (i = 0; i < n; i++) {
                line = String.format("%d ", i+1);
                for (j = 0; j < n; j++) {
                    if (sn.getStatus(i, j) == Element.PREDEF) {
                        v = sn.getValue(i, j);
                    } else {
                        v = 0;
                    }
                    line += String.format(" %d", v);
                }
                line += "\n";
                fr.write(line);
            }
            fr.write("###########\n");
            handlist = sn.getHandlist();
            char cmd;
            for (Hand h: handlist.getHans()) {
                cmd = h.cmd();
                if (cmd != 'I') {
                    line = String.format("%d %c %d %d %d\n", h.no(), h.cmd(), h.x(), h.y(), h.v());
                    i++;
                    fr.write(line);
                }
            }

            fr.write("##Current State##\n");
            for (i = 0; i < n; i++) {
                line = String.format("## %d ", i+1);
                for (j = 0; j < n; j++) {
                    v = sn.getValue(i, j);
                    line += String.format(" %d", v);
                }
                line += "\n";
                fr.write(line);
            }
            fr.write("###########\n");

            sn.setFilenmae(f.getName());
        } catch (IOException ioe) {
            System.err.println("ERROR:" + ioe);
        }
    }

    public void reset(SudokuNumbers sn) {
        sn.reset();
    }
}
