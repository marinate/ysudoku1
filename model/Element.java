package model;

public class Element {
    public static final int NULL  = 0;
    public static final int PREDEF = 1;
    public static final int SETTING = 2;
    public static final int DELETING = 3;
    public static final int FIXED = 4;

    private int val;
    private int status;

    public Element(int val, int stauts) {
        this.val = val;
        this.status = status;
    }

    private String getStatueName(int s) {
        String str = "Unknown";
        switch (s) {
        case NULL:
            str = "NULL";
            break;
        case PREDEF:
            str = "PREDEF";
            break;
        case SETTING:
            str = "SETTING";
            break;
        case DELETING:
            str = "DELETING";
            break;
        case FIXED:
            str = "FIXED";
            break;
        }
        return str;
    }

    public void setVal(int val) {
        this.val = val;
    }

    public void setVala(int val) {
        this.val = val;
        if (val == 0)
            status = NULL;
        else
            status = FIXED;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setValStatus(int val, int status) {
        this.val = val;
        this.status = status;
    }

    public int val() {
        return val;
    }

    public int status() {
        return status;
    }

    public String toString() {
        return "(" + val + ", " + getStatueName(status) + ")";
    }
}