package model;

import java.util.List;

public class ValuePos {
    private int area;
    private int value;
    private int x;
    private int y;

    public ValuePos(int area, int val, int x, int y) {
        this.area = area;
        this.value = val;
        this.x = x;
        this.y = y;
    }

    public int area() {
        return area;
    }

    public int value() {
        return value;
    }

    public int x() {
        return x;
    }

    public int y() {
        return y;
    }

    private String printf() {
        String ret = String.format("VP(%d)[ %d : %d, %d]", area, value, x + 1, y + 1);
        return ret;
    }

    public void print() {
        String p = printf();
        System.out.print(p + "%n");
    }

    public void print(String s) {
        String p = printf();
        System.out.print(p + s);
    }

    public static void printList(List<ValuePos> vlist) {
        System.out.println("------->");
        if (vlist != null) {
            for (ValuePos vp : vlist) {
                vp.print();
            }
        }
        System.out.println("-------<");
    }
}
