package model;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

public class Hint {
    private SudokuNumbers sn;
    private int n;
    private int debug = 0;

    public Hint(SudokuNumbers sn) {
        this.sn = sn;
        this.n = sn.n();
    }

    public int[][] getHint(int k) {
        int[][] h = new int[n][n];
        int i, j;
        int x, y, z;

        System.out.println("----Hint-----");
        for (z = 1; z <= n; z++) {
            x = -1;
            y = -1;
            for (i = 0; i < n; i++) {
                for (j = 0; j < n; j++) {
                    h[i][j] = sn.getValue(i, j);
                }
            }
            for (i = 0; i < n; i++) {
                for (j = 0; j < n; j++) {
                    if (sn.getValue(i, j) == z) {
                        x = i;
                        y = j;
                        setarea(h, x, y, z);
                        //System.out.println("K=" + z + "  (" + x + ", " + y);
                        break;
                    }
                }
            }
            //System.out.println("-----HINT-------Z=" + z);
            //for (i = 0; i < n; i++) {
            //    for (j = 0; j < n; j++) {
            //        System.out.print(" " + h[i][j]);
            //    }
            //    System.out.println();
            //}
            checkhint(h, z);
        }
        
        for (i = 0; i < n; i++) {
            for (j = 0; j < n; j++) {
                h[i][j] = sn.getValue(i, j);
            }
        }
        checkhint1(h);
        return null;
    }

    private void setarea(int[][] h, int x, int y, int k) {
        int i, j;

        for (j = 0; j < n; j++) {
            h[x][j] = k;
        }
        for (i = 0; i < n; i++) {
            h[i][y] = k;
        }
        int nx = (x / 3) * 3;
        int ny = (y / 3) * 3;
        for (i = nx; i < nx + 3; i++) {
            for (j = ny; j < ny + 3; j++) {
                if (h[i][j] == 0)
                    h[i][j] = k;
            }
        }
    }

    private void checkhint1(int[][] h) {
        int i, j;

        for (i = 0; i < n; i++) {
            checkhint1s(h, i, i+1, 0, n);
        }
        for (i = 0; i < n; i++) {
            checkhint1s(h, 0, n, i, i+1);
        }
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                checkhint1s(h, i*3, i*3 + 3, j*3, j*3 + 3);
            }
        }
    }

    private void checkhint1s(int[][] h, int i1, int i2, int j1, int j2) {
        int i, j, k, a;
        int[] av = new int[n+1];
        int[] ax = new int[n+1];
        int[] ay = new int[n+1];
        int nz;

        //System.out.println("--HIST1S i1=" + i1 + "," + i2 + "   j1=" + j1 + "," + j2);
        for (k = 0; k <= n; k++) {
            av[k] = 0;
            ax[k] = 0;
            ay[k] = 0;
        }
        nz = 0;
        for (i = i1; i < i2; i++) {
            for (j = j1; j < j2; j++) {
                a = h[i][j];
                if (a == 0)
                    nz++;
                av[a] = a;
                ax[a] = i;
                ay[a] = j;
            }
        }
        //System.out.println("A=[" + av[0] + "," + av[1] + "," + av[2] + "," + av[3] + "," + av[4] + "," + av[5] + "," + av[6] + "," + av[7] + "," + av[8] + "," + av[9] + "]");
        if (nz == 1) {
            for (k = 1; k <= n; k++) {
                if (av[k] == 0) {
                       System.out.println("HINT1: [" + (ax[0] +1) + ", " + (ay[0] + 1) + "   " + k + "]");
                }
            }
        }
    }

    private void checkhint(int[][] h, int k) {
        int i, j;
        int x, y;
        int px, py;
        int n;
        //System.out.println("HINT " + k);
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                n = 0;
                px = 0;
                py = 0;
                for (x = i * 3; x < i*3 + 3; x++) {
                    for (y = j*3; y < j*3 + 3; y++) {
                        if (h[x][y] == 0) {
                            px = x;
                            py = y;
                            n++;
                        }
                    }
                }
                //System.out.println("K=" + k + "  n=" + n);
                if (n == 1) {
                    System.out.println("HINT: [" + (px +1) + ", " + (py + 1) + "   " + k + "]");
                }
            }
        }
    }
}
