package model;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

public class Check {
    private SudokuNumbers sn;
    private int n;
    private int debug = 0;

    public Check(SudokuNumbers sn) {
        this.sn = sn;
        this.n = sn.n();
    }

    public List<ValuePos> check() {
        int id = 1;
        int i, j;
        int ok = 1;
        List<ValuePos> elist = new ArrayList<>();
        List<ValuePos> elist0 = null;

        if (debug > 0)
            System.out.println("---------CHECK ---------------------");
        for (i = 0; i < n; i++) {
            elist0 = checkone(id, i, i + 1, 0, n);
            if (elist0 != null) {
                ok = 0;
                elist.addAll(elist0);
            }
            if (debug > 0)
                System.out.println("--check id=" + id + "  ok=" + ok);
            id++;
        }
        for (j = 0; j < n; j++) {
            elist0 = checkone(id, 0, n, j, j + 1);
            if (elist0 != null) {
                ok = 0;
                elist.addAll(elist0);
            }
            if (debug > 0)
                System.out.println("--check id=" + id + "  ok=" + ok);
            id++;
        }
        for (i = 0; i < n; i += 3) {
            for (j = 0; j < n; j += 3) {
                elist0 = checkone(id, i, i + 3, j, j + 3);
                if (elist0 != null) {
                    ok = 0;
                    elist.addAll(elist0);
                }
                if (debug > 0)
                    System.out.println("--check id=" + id + "  ok=" + ok);
                id++;
            }
        }
        if (ok == 1)
            elist = null;
        return elist;
    }

    private List<ValuePos> checkone(int id, int i1, int i2, int j1, int j2) {
        int i, j, k, key, nstatus;
        List<ValuePos> vp;
        Map<Integer, List<ValuePos>> map = new HashMap<>();

        for (i = 0; i <= n; i++) {
            map.put(Integer.valueOf(i), new ArrayList<ValuePos>());
        }
        for (i = i1; i < i2; i++) {
            for (j = j1; j < j2; j++) {
                  nstatus = sn.getStatus(i, j);
                 if (nstatus != Element.NULL) {
                     k = sn.getValue(i, j);
                     key = k;
                     vp = map.get(Integer.valueOf(key));
                     if (vp == null)
                         vp = new ArrayList<ValuePos>();
                     vp.add(new ValuePos(id, k, i, j));
                 }
            }
        }

        if (debug > 0) {
            System.out.println(">>>>>>>>>>>>>>>" + id);
            for (Integer ii : map.keySet()) {
                vp = map.get(ii);
                if (vp.size() > 0) {
                    System.out.print(ii.intValue() + ":");
                    for (ValuePos pp : vp) {
                        pp.print(" ");
                    }
                    System.out.println();
                }
            }
            System.out.println("  ---");
        }

        // check
        int ok = 1;
        int nv = 0;
        List<ValuePos> elist = new ArrayList<ValuePos>();
        for (i = 1; i <= n; i++) {
            vp = map.get(Integer.valueOf(i));
            nv = vp.size();
            if (nv != 1) {
                ok = 0;
            }
            if (nv > 1) {
                elist.addAll(vp);
            }
        }

        if (debug > 0) {
            System.out.println("----- " + id + "-----  ok=" + ok);
            for (ValuePos pp : elist) {
                pp.print(" ");
            }
            System.out.println();
        }
        if (ok == 1)
            elist = null;
        return elist;
    }
}
