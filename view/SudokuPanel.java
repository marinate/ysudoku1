package view;

import java.util.List;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JPanel;
import model.SudokuNumbers;
import model.Element;
import model.ValuePos;

class SudokuPanel extends JPanel implements MouseListener, ActionListener {
    private SudokuBoard sb;
    private SudokuNumbers sn;
    private int width;
    private int height;
    private int pieacew;
    private String[] vallist;
    private SJComboBox comb;
    private Font font1, fontb;
    private Color predefc = Color.LIGHT_GRAY;
    private Color combbgc;
    private List<ValuePos> elist;
    private int errdisp = 1;
    private int debug = 0;

    public SudokuPanel(SudokuBoard sb) {
        int i, j, n;

        this.sb = sb;
        this.sn = sb.sn();
        n = sn.n();
        vallist = sn.vallist();
        elist = new ArrayList<ValuePos>();
        pieacew = sb.pieacew();
        width = n * pieacew;
        height = n * pieacew;
        setLayout(null);
        setSize(width, height);
        addMouseListener(this);
        font1 = new Font("Arial", Font.PLAIN, 24);
        fontb = new Font("Arial", Font.BOLD, 24);
        comb = new SJComboBox(vallist);
        comb.setFont(font1);
        comb.setOpaque(true);
        comb.setPreferredSize(new Dimension(40, n * 40+1000));
        comb.addActionListener(this);
        comb.setVisible(false);
        combbgc = comb.getBackground();
        add(comb);
    }

    public void reset() {
        vallist = sn.vallist();
        elist = new ArrayList<ValuePos>();
        comb.setVisible(false);
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void setElist(List<ValuePos> elist) {
        this.elist = elist;
    }

    public void paint(Graphics g) {
        Graphics2D g2 = (Graphics2D)g;
        super.paint(g);
        int i, j, n;
        int x, y;
        BasicStroke bs5= new BasicStroke(5);
        Stroke bs0;
        bs0 = g2.getStroke();
        n = sn.n();
        int xtoffset = 20;
        int ytoffset = 36;
        Color fgc = g.getColor();
        Color errorc = Color.RED;
 
        int combx = -1;
        int comby = -1;
        if (comb.isVisible()) {
            combx = comb.x();
            comby = comb.y();
            comb.setBackground(combbgc);
        }

        for (i = 0; i < n; i++) {
            for (j = 0; j < n; j++) {
                x = j * pieacew;
                y = i * pieacew;
                boolean errpos = false;
                if (errdisp == 1 && elist != null) {
                    for (ValuePos e : elist) {
                        if (e.x() == i && e.y() == j) {
                            errpos = true;
                            break;
                        }
                    }
                }

                if (sn.getStatus(i, j) == Element.PREDEF) {
                    g2.setColor(predefc);
                    g2.fillRect(x+1, y+1, pieacew-2, pieacew-2);
                    g2.setFont(fontb);
                } else {
                    g2.setFont(font1);
                }

                if (errpos) {
                    g2.setColor(errorc);
                    g2.fillRect(x+2, y+2, pieacew-4, pieacew-4);
                    if (j == comby && i == combx)
                        comb.setBackground(errorc);
                }
                g2.setColor(fgc);
                if (j != comby || i != combx) {
                    g2.drawString(vallist[sn.getValue(i, j)], x+xtoffset, y+ytoffset);
                }
            }
        }

        for (i = 0; i <= n; i++) {
            if (i % 3 == 0) {
                g2.setStroke(bs5);
                g.drawLine(i * pieacew, 0, i * pieacew, height);
           } else {
                g2.setStroke(bs0);
                g.drawLine(i * pieacew, 0, i * pieacew, height);
           }
        }
        for (i = 0; i <= n; i++) {
            if (i % 3 == 0) {
                g2.setStroke(bs5);
                g.drawLine(0, i * pieacew, width, i * pieacew);
            } else {
                g2.setStroke(bs0);
                g.drawLine(0, i * pieacew, width, i * pieacew);
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        SJComboBox scomb = (SJComboBox)e.getSource();
        int x = scomb.x();
        int y = scomb.y();
        int v = scomb.getSelectedIndex();
        //System.out.println("COMB.AE x=" + x + ", y=" + y + ", v=" + v);
        sb.eventhand(x, y, v);
        scomb.setVisible(false);
        repaint();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        Point p = e.getPoint();
        int x, y, i, j;
        int status;
        j = (int)p.getX() / pieacew;
        i = (int)p.getY() / pieacew;
        x = j * pieacew;
        y = i * pieacew;
        status = sn.getStatus(i, j);
        if (status == Element.NULL || status == Element.FIXED) {
            comb.setXY(i, j);
            comb.setSelectedIndex(sn.getValue(i, j));
            comb.setBounds(x, y, pieacew, pieacew);
            comb.setVisible(true);
            revalidate();
            repaint();
        } else {
            comb.setVisible(false);
        }
        //System.out.println("MOUSE-CLICKED  x=" + x + ", " + y + " => (" + i + ", " + j + ")  STATUS=" + status);
    }

    @Override
    public void mousePressed(MouseEvent e) {
        //System.out.println("MOUSEPRESSED");
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        //System.out.println("MOUSERLEASED");
    }
    @Override
    public void mouseEntered(MouseEvent e) {
        //System.out.println("MOUSEENTERED");
    }
    @Override
    public void mouseExited(MouseEvent e) {
        //System.out.println("MOUSEExited");
    }

}
