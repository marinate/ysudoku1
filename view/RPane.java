package view;

import java.util.List;
import java.awt.Color;
import java.awt.BorderLayout;
import java.awt.Point;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import model.Hand;
import model.Handlist;

public class RPane extends JPanel {
    private JTextField textf;
    private JTextArea history;
    private JTextField lastput;
    private JScrollPane scroll;

    public RPane(int width, int height) {
        setSize(width, height);
        textf = new JTextField();
        lastput = new JTextField();
        history = new JTextArea(20, 1);
        scroll = new JScrollPane(history, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        setBackground(Color.GREEN);
        setLayout(new BorderLayout());
        add(textf, BorderLayout.NORTH);
        add(scroll, BorderLayout.CENTER);
        add(lastput, BorderLayout.SOUTH);
    }

    public void setTextf(String text) {
        textf.setText(text);
        //System.out.println("TESTF=" + text);
        repaint();
    }

    public void setTexta(String text, int cpos) {
        history.setText(text);
        if (cpos >= 0) {
            history.setCaretPosition(cpos);
        }
        //System.out.println("TESTA=" + text);
        repaint();
    }

    public void setTextlastput(String text) {
        lastput.setText(text);
        repaint();
    }

    public void dispHanlist(Handlist handlist) {
        int all = 1;
        String strh = "";
        String lastput = "";
        String hs;
        int cpos = 0;
        for (Hand h : handlist.getHans()) {
            if (all == 1 || h.cmd() != 'I') {
                hs = h.toString();
                if (h.no() == handlist.curid()) {
                    strh += "*";
                    cpos += 1;
                    lastput = hs;
                }
                strh +=  hs + "\n";
                if (lastput.equals(""))
                    cpos += hs.length() + 1;
            }
        }
        setTexta(strh, cpos);
        setTextlastput(lastput);
    }
}
