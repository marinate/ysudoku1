package view;

import java.io.File;
import java.util.List;
import model.Check;
import model.SudokuNumbers;
import model.Fileio;
import model.ValuePos;

public class SudokuMainView {
    private SudokuNumbers sn;
    private Check ck;
    private SudokuBoard sb;
    private Fileio fio;
    private String openedfilename;
    private String openedabsfilename;

    public SudokuMainView() {
        fio = new Fileio();
    }

    public void start() {
        sn = new SudokuNumbers();
        ck = new Check(sn);

        String initdata = "1,4:4\n1,6:6\n" +
            "2,1:7\n2,2:4\n2,8:8\n2,9:2\n" +
            "3,2:9\n3,4:2\n3,6:3\n3,8:6\n" +
            "4,1:8\n4,3:1\n4,7:2\n4,9:4\n" +
            "6,1:5\n6,3:4\n6,7:1\n6,9:9\n" +
            "7,2:7\n7,4:9\n7,6:2\n7,8:4\n" +
            "8,1:4\n8,2:1\n8,8:9\n8,9:6\n" +
            "9,4:1\n9,6:8\n";
        sn.setInitialdata("");
        sb = new SudokuBoard(sn, this);
        sb.setVisible(true);
    }

    public List<ValuePos> snew(File file) {
        if (file != null) {
            //openedabsfilename = file.getAbsolutePath();
            //openedabsfilename = openedabsfilename + ".csv";
            //openedfilename = file.getName() + ".csv";
            openedabsfilename = "";
            openedfilename = "";
            return fio.newhand(sn, file);
        }
        return null;
    }

    public List<ValuePos> sopen(File file) {
        if (file != null) {
            openedabsfilename = file.getAbsolutePath();
            openedfilename = file.getName();
            return fio.openhand(sn, file);
        }
        return null;
    }

    public void ssave(File file) {
        if (file != null) {
            openedabsfilename = file.getAbsolutePath();
            openedfilename = file.getName();
            fio.savehand(sn, file);
        }
    }

    public void sreset() {
        fio.reset(sn);
    }
    public void sclose() {
        System.out.println("MAIN CLODE");
    }

    public String openedfilenmae() {
        return openedfilename;
    }

    public String openedabsfilenmae() {
        return openedabsfilename;
    }

    public void forward() {
        sn.forward();
    }

    public void backword() {
        sn.backword();
    }
}
