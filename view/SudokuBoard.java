package view;
import java.util.List;
import java.util.ArrayList;
import java.io.File;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.color.*;

import javax.swing.JMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JFrame;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JTextField;
import model.SudokuNumbers;
import model.Hand;
import model.ValuePos;

public class SudokuBoard extends JFrame implements ActionListener {
    private int n;
    private int width, height;
    private SudokuPanel sp;
    private int pieacew = 50;
    private SudokuNumbers sn;
    private String[] vallist;
    private List<ValuePos> elist;
    private JMenuItem menunew, menuopen, menusave, menusaveas, menuclose, menureset;
    private JButton fwbtn;
    private JButton bwbtn;
    private JButton dbgbtn;
    private SudokuMainView smv;
    private RPane rpane;
    private int debug = 0;

    public SudokuBoard(SudokuNumbers sn, SudokuMainView smv) {
        this.smv = smv;
        this.sn = sn;
        this.n = sn.n();
        this.vallist = sn.vallist();
        this.elist = new ArrayList<ValuePos>();
        int rpanewidth = 120;
        sp = new SudokuPanel(this);
        width = sp.getWidth();
        height = sp.getHeight();
        setSize(width + rpanewidth + 20, height + 100);
        setTitle("Sudoku");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        rpane = new RPane(rpanewidth, height);
        setLayout(null);
        add(sp);
        rpane.setLocation(width, 0);
        add(rpane);

        JPanel jp0 = new JPanel();
        jp0.setBounds(width, height, rpane.getWidth(), 50);
        fwbtn = new JButton(">");
        fwbtn.setActionCommand("forward");
        fwbtn.addActionListener(this);
        bwbtn = new JButton("<");
        bwbtn.setActionCommand("backword");
        bwbtn.addActionListener(this);
        jp0.add(bwbtn);
        jp0.add(fwbtn);
        add(jp0);

        JPanel jp2 = new JPanel();
        jp2.setBounds(0, height, 100, 50);
        dbgbtn = new JButton("DBG");
        dbgbtn.addActionListener(this);
        jp2.add(dbgbtn);
        add(jp2);

        // MenuBar
        JMenuBar mb = new JMenuBar();
        JMenu mfile = new JMenu("File");
        mb.add(mfile);
        menunew = new JMenuItem("New");
        menunew.setActionCommand("NEW");
        menunew.addActionListener(this);
        menuopen = new JMenuItem("Open");
        menuopen.setActionCommand("OPEN");
        menuopen.addActionListener(this);
        menusave = new JMenuItem("Save");
        menusave.setActionCommand("SAVE");
        menusave.addActionListener(this);
        menusaveas = new JMenuItem("Save As");
        menusaveas.setActionCommand("SAVEAS");
        menusaveas.addActionListener(this);
        menuclose = new JMenuItem("Close");
        menuclose.setActionCommand("CLOSE");
        menuclose.addActionListener(this);
        menureset = new JMenuItem("Reset");
        menureset.setActionCommand("RESET");
        menureset.addActionListener(this);
        mfile.add(menunew);
        mfile.add(menuopen);
        mfile.add(menusave);
        mfile.add(menusaveas);
        mfile.add(menureset);
        mfile.add(menuclose);
        setJMenuBar(mb);
    }

    public SudokuNumbers sn() {
        return sn;
    }

    public int pieacew() {
        return pieacew;
    }

    public String cnvStrElist() {
        String s = "";
        if (elist == null) {
            s = "DONE";
        } else if (elist.size() > 0) {
            s = "ERROR";
        } else {
            s = "OK";
        }
        return s;
    }

    public void setHeadtitle() {
        String htitle = sn.title() + "     (" + sn.filename() + ")";
        setTitle(htitle);
    }
    public void reset() {
        setHeadtitle();
        sp.reset();
    }

    public void eventhand(int x, int y, int v) {
        int rc = sn.putNum(x+1, y+1, v);
        sn.confirm();
        elist = sn.check();
        sp.setElist(elist);
        rpane.dispHanlist(sn.getHandlist());
        rpane.setTextf(cnvStrElist());
        //System.out.println("EVENTHAND  X=" + x + ", " + y + " v=" +  v + "   rc=" + rc);
        //sn.dumpHanlist(0);
        if (elist != null) {
            if (debug > 0) {
                System.out.println("----ERROR LIST -----");
                for (ValuePos el : elist) {
                    el.print(" ");
                }
                System.out.println("------");
            }
        }
        rpane.repaint();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String cmd = e.getActionCommand();
        File file;
        String savefilename;

        if (e.getSource() == menunew) {
            JFileChooser filechooser = new JFileChooser();
            int selected = filechooser.showOpenDialog(this);
            file = filechooser.getSelectedFile();
            elist = smv.snew(file);
            rpane.setTextf(cnvStrElist());
            rpane.dispHanlist(sn.getHandlist());
            //ValuePos.printList(elist);
            reset();
            sp.repaint();
        } else if (e.getSource() == menuopen) {
            JFileChooser filechooser = new JFileChooser();
            int selected = filechooser.showOpenDialog(this);
            file = filechooser.getSelectedFile();
            elist = smv.sopen(file);
            rpane.setTextf(cnvStrElist());
            rpane.dispHanlist(sn.getHandlist());
            //ValuePos.printList(elist);
            reset();
            sp.setElist(elist);
            sp.repaint();
        } else if (e.getSource() == menusave) {
            savefilename = smv.openedabsfilenmae();
            if (savefilename == null || savefilename.equals("")) {
                JFileChooser filechooser = new JFileChooser();
                int selected = filechooser.showOpenDialog(this);
                file = filechooser.getSelectedFile();
            } else {
                file = new File(savefilename);
            }
            smv.ssave(file);
            setHeadtitle();
        } else if (e.getSource() == menusaveas) {
            JFileChooser filechooser = new JFileChooser();
            int selected = filechooser.showOpenDialog(this);
            file = filechooser.getSelectedFile();
            if (file != null && file.exists()) {
                switch (JOptionPane.showConfirmDialog(this, 
                    file.getName() + " is already exits.\nOverwrite?",
                    "Confirm overwrite", JOptionPane.YES_NO_CANCEL_OPTION)) {
                case JOptionPane.YES_OPTION:
                    break;
                case JOptionPane.NO_OPTION:
                    break;
            case JOptionPane.CANCEL_OPTION:
                return;
        }
            }
            smv.ssave(file);
            setHeadtitle();
        } else if (e.getSource() == menureset) {
            smv.sreset();
            reset();
            sp.repaint();
        } else if (e.getSource() == menuclose) {
            smv.sclose();
        } else if (e.getSource() == fwbtn) {
            smv.forward();
            elist = sn.check();
            sp.setElist(elist);
            sp.repaint();
            rpane.setTextf(cnvStrElist());
            rpane.dispHanlist(sn.getHandlist());
        } else if (e.getSource() == bwbtn) {
            smv.backword();
            elist = sn.check();
            sp.setElist(elist);
            sp.repaint();
            rpane.setTextf(cnvStrElist());
            rpane.dispHanlist(sn.getHandlist());
        } else if (e.getSource() == dbgbtn) {
            sn.dumpHanlist(1);
            sn.hint();
        }
    }
}
