package view;
import javax.swing.JComboBox;
public class SJComboBox extends JComboBox<String> {
    private int x;
    private int y;

    public SJComboBox(String[] e) {
        super(e);
    }

    public int x() {
        return x;
    }

    public int y() {
        return y;
    }

    public void setXY(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }
}
